import { Environment, MeshReflectorMaterial, OrbitControls, Preload, useProgress } from '@react-three/drei'
import { Perf } from 'r3f-perf'
import { Suspense, useState } from 'react'
import Frames from './components/Frames.jsx'
import { Canvas } from '@react-three/fiber'
import { useEffect } from 'react'

export default function CanvasGallery({loadingComplete, setLoadingComplete, setPosterName})
{
    const { progress } = useProgress();

    useEffect(() => {
        if (progress === 100 && !loadingComplete) {
            setLoadingComplete(true);
        }
    }, [progress, loadingComplete, setLoadingComplete]);

    return <Canvas
        dpr={[1, 1.5]} 
        shadows={false}
        camera={ {
            fov: 70,
            position: [ 0, 0.6, 4.8 ]
        } }
        >
        <Suspense fallback={null}>
            <color attach="background" args={['#191920']}/>
            <fog attach="fog" args={['#191920', 0, 15]} />

            {/* <Perf position="top-left" /> */}

            {/* <OrbitControls makeDefault /> */}

            {/* <ambientLight intensity={ 1.5 } /> */}

            <Environment preset="city" background={false} />
            <group position={[0, -0.5, 0]}>
                <Frames setPosterName={setPosterName}/>
                <mesh rotation={[-Math.PI / 2, 0, 0]}>
                    <planeGeometry args={[50, 50]}/>
                    <MeshReflectorMaterial
                        blur={[300, 100]}
                        resolution={1024}
                        mixBlur={1}
                        mixStrength={70}
                        roughness={1}
                        depthScale={1.2}
                        minDepthThreshold={0.4}
                        maxDepthThreshold={1.4}
                        color="#050505"
                        metalness={0.5}
                        />
                </mesh>
            </group>
        </Suspense>

    </Canvas>
}