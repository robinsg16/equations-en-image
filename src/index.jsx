import { useEffect, useState } from 'react'
import ReactDOM from 'react-dom/client'
import CanvasGallery from './Experience.jsx'
import About from './components/About.jsx'
import Infos from './components/Infos.jsx'
import Landing from './components/Landing.jsx'
import Navbar from './components/Navbar.jsx'
import './style.css'
import RotateScreen from './components/RotateScreen.jsx'

const root = ReactDOM.createRoot(document.querySelector('#root'))

const App = () => {
    const [loadingComplete, setLoadingComplete] = useState(false);
    const [aboutVisible, setAboutVisible] = useState(false);
    const [posterName, setPosterName] = useState('');
    const [isPortrait, setIsPortrait] = useState(window.innerHeight > window.innerWidth);

    useEffect(() => {
      const handleResize = () => {
        setIsPortrait(window.innerHeight > window.innerWidth);
      };
  
      // Initial check
      handleResize();
  
      // Add event listener for resize (orientation change)
      window.addEventListener('resize', handleResize);
  
      // Cleanup the event listener on component unmount
      return () => {
        window.removeEventListener('resize', handleResize);
      };
    }, []);

    return (
        <>
            {aboutVisible && <About aboutVisible={aboutVisible} setAboutVisible={setAboutVisible}/>}
            <Landing loadingComplete={loadingComplete}/>
            {isPortrait && <RotateScreen/>}
            <Navbar aboutVisible={aboutVisible} setAboutVisible={setAboutVisible}/>
            {posterName!== '' && <Infos posterName={posterName}/>}
            <CanvasGallery loadingComplete={loadingComplete} setLoadingComplete={setLoadingComplete} setPosterName={setPosterName}/>
        </>
    )
}

root.render(
    <App/>
)