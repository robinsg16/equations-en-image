import { Image, useCursor } from '@react-three/drei'
import { useFrame, useLoader } from '@react-three/fiber'
import { easing } from 'maath'
import { useEffect, useRef, useState } from 'react'
import * as THREE from 'three'
import { useLocation, useRoute } from 'wouter'
import images from '../datas/Posters'

const GOLDENRATIO = 1.61803398875

export default function Frames({ q = new THREE.Quaternion(), p = new THREE.Vector3(), setPosterName }) {
  const ref = useRef()
  const clicked = useRef()
  const [, params] = useRoute('/poster/:id')
  const [, setLocation] = useLocation()

  useEffect(() => {
    clicked.current = ref.current.getObjectByName(params?.id)
    if (clicked.current) {
      clicked.current.parent.updateWorldMatrix(true, true)
      clicked.current.parent.localToWorld(p.set(0, GOLDENRATIO / 2, 0.85))
      clicked.current.parent.getWorldQuaternion(q)
    } else {
      p.set(0, 0, 5.5)
      q.identity()
    }
  })

  useFrame((state, dt) => {
    easing.damp3(state.camera.position, p, 0.4, dt)
    easing.dampQ(state.camera.quaternion, q, 0.4, dt)
  })
  
  return (
    <group
      ref={ref}
      onClick={(e) => (e.stopPropagation(), setLocation(clicked.current === e.object ? '/' : '/poster/' + e.object.name), setPosterName(clicked.current === e.object ? '/' : e.object.name))}
      onPointerMissed={() => (setLocation('/'), setPosterName(''))}>
      {images.map((props) => <Frame key={props.url} {...props} />)}
    </group>
  )
}

function Frame({ url, name, c = new THREE.Color(), ...props }) {
  const image = useRef()
  const frame = useRef()
  const [, params] = useRoute('/poster/:id')
  const [hovered, hover] = useState(false)
  const [rnd] = useState(() => Math.random())
  // const name = getUuid(url)
  const isActive = params?.id === name

  useLoader(THREE.TextureLoader, url);

  useCursor(hovered)
  useFrame((state, dt) => {
    // image.current.material.zoom = 0 + Math.sin(rnd * 10000 + state.clock.elapsedTime / 3) / 2
    // easing.damp3(image.current.scale, [0.85 * (!isActive && hovered ? 0.85 : 1), 0.9 * (!isActive && hovered ? 0.905 : 1), 1], 0.1, dt)
    easing.dampC(frame.current.material.color, hovered ? 'white' : 'black', 0.1, dt)
  })
  return (
    <group {...props}         
      onPointerOver={(e) => (e.stopPropagation(), hover(true))}
      onPointerOut={() => hover(false)}
    >
      <mesh
        name={name}
        // scale={[1.2, 1.2, 0.05]}
        position={[0, 0.6, 0]}
        >
        <boxGeometry args={[0.8, 1.5, 0.08]}/>
        <meshStandardMaterial transparent color="#151515" metalness={0.5} roughness={0.5} envMapIntensity={2} />
        <mesh ref={frame} raycast={() => null} scale={[0.66, 0.93, 0.02]} position={[0, 0.2, 0.1]}>
          <boxGeometry />
          <meshBasicMaterial toneMapped={false} fog={false} />
        </mesh>
        <Image raycast={() => null} ref={image} position={[0, 0.2, 0.12]} url={url}  scale={[0.62, 0.9]}/>
      </mesh>
      {/* <Text maxWidth={0.1} anchorX="left" anchorY="top" position={[0.55, GOLDENRATIO, 0]} fontSize={0.025}>
        {name.split('-').join(' ')}
      </Text> */}
    </group>
  )
}
