const RotateScreen = () => {

    return (
        <div className="rotateScreen">
            <img className="rotateImg" src="/img/rotate_phone.svg" alt="Please rotate your screen"/>
        </div>
    )
}

export default RotateScreen;