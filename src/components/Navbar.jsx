const Navbar = ({aboutVisible, setAboutVisible}) => {
    const handleAboutClick = () => {
        setAboutVisible(true);
    }

    return (
        <>
        <div className="navbar">
            <div className="content">
                <p>Équations en image</p>
                <p className="aboutButton" onClick={handleAboutClick}>À propos</p>
            </div>
        </div>
        </>
    )
}

export default Navbar;