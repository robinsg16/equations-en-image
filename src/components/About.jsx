const About = ({aboutVisible, setAboutVisible}) => {
    const handleButtonClick = () => {
        setAboutVisible(false);
    };

    return (
        <div className="about">
            <div className="returnButton" onClick={handleButtonClick}>Retour</div>
            <div className="content">
                <div className="leftBloc">
                    <img className="photo" src="/img/expo-resize.webp" alt="Photo de l'exposition"/>
                    <div className="teamTitle">Equipe du projet</div>
                    <p className="team">
                        Design graphique : Sacha Chouvin, Julia Raybaudi, Fabian Santiago et Robin Sause--Gautier <br/>
                        Direction scientifique et artistique : Vincent Nozick et Gaëtan Robillard <br/>
                        Formation IMAC - ESIEE (ex-ESIPE) <br/>
                        Soutiens: Actions Incitatives en Pédagogie, Mission Arts & Culture, Université Gustave Eiffel, La Centrif’
                    </p>
                </div>
                <div className="rightBloc">
                    <p className="title">Equations <br/> en image</p>
                    <p className="text">
                        Formes visuelles et notations scientifiques entretiennent de nombreux rapports. 
                        Entre arts et sciences, Équations en Image est un travail d’affiches qui met en 
                        lumière certaines relations trop souvent éclipsées par la séparation des disciplines. 
                        Dans le cadre d'un projet tutoré de la formation IMAC ESIEE (ex-ESIPE), une équipe 
                        d’enseignants, d’étudiants et de chercheurs de l’Université Gustave Eiffel s’est 
                        formée pour produire et exposer une œuvre graphique. Une série de huit grands formats 
                        a été réalisée pour mettre en image des concepts ou des intentions scientifiques – 
                        une démarche qui s’appuie sur une enquête engagée auprès du laboratoire d'informatique 
                        Gaspard Monge (LIGM) et du Laboratoire d’analyse et de mathématiques appliquées (LAMA). 
                        L’idée que le langage scientifique ouvre à une pensée visuelle en soi a d’ailleurs été 
                        appuyée sur une étude parallèle des travaux de György Kepes, et notamment de son ouvrage 
                        The New Landscape in Art and Science publié en 1956 à Chicago. Dans le travail d’affiche 
                        résultant, cette iconographie a nourri un vocabulaire : lumière, transparence, courbe, 
                        grille, symétrie, rythme… Ainsi comment les images visuelles participent-elles à la 
                        connaissance ? Même si les affiches n’ont pas pour objectif la compréhension stricte des 
                        équations en jeu, elles sont pensées pour interroger et susciter la curiosité tout en promouvant 
                        le lien art-science dans la transmission de la connaissance, de la culture universitaire et de la recherche.
                    </p>
                </div>
            </div>
        </div>
    )
}

export default About;