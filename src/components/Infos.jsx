import Latex from 'react-latex';
import posters from '../datas/Posters';
import { useEffect } from 'react';
import { useState } from 'react';

const Infos = ({ posterName }) => {
    const [visible, setVisible] = useState(false);
    const poster = posters.find((p) => p.name === posterName);

    useEffect(() => {
        // Lorsque posterName change, réinitialiser visible à false
        setVisible(false);
        const timer = setTimeout(() => {
            if (poster) {
                setVisible(true); // Définir visible à true après une courte pause
            }
        }, 15); // Petite pause pour permettre à la transition de redémarrer

        return () => clearTimeout(timer); // Nettoyer le timeout lors du démontage
    }, [posterName]);

    if (poster) 
    return (
        <div className={`infos ${visible ? 'slide-in' : ''}`}>
            <div className='content' 
            // style={{border: '5px solid ${poster.color}'}}
            >
                <div className='title'>{poster.title}</div>
                <div className='subtitle'>{poster.subtitle}</div>
                <div className='equation'>
                    <Latex displayMode={true}>
                        {poster.equation}
                    </Latex>
                </div>
                <p className='text'>
                    {poster.text}
                </p>

                {/* <div className='blocNom'>
                    <p>Design graphique</p>
                    <div className='ligne'>
                        <div className='colonne'>
                            <div>Sacha Chouvin</div>
                            <div>Julia Raybaudi</div>
                        </div>
                        <div className='colonne'>
                            <div>Fabian Santiago</div>
                            <div>Robin Sause--Gautier</div>
                        </div>
                    </div>
                </div>

                <div className='blocNom'>
                    <p>Direction scientifique et artistique</p>
                    <div className='colonne'>
                        <div>Vincent Nozick</div>
                        <div>Gaëtan Robillard</div>
                    </div>
                </div> */}
            </div>
        </div>
    )
}

export default Infos;