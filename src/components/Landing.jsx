import { useEffect, useState } from "react";
import LoadingDots from "./LoadingDots";

const Landing = ({loadingComplete}) => {
    const [isVisible, setIsVisible] = useState(true);
    const [showButton ,setShowButton] = useState(false);
    const [currentImageIndex, setCurrentImageIndex] = useState(3);

    const handleButtonClick = () => {
      // Set the visibility state to false to make the page slide up and disappear
      setIsVisible(false);
    };

    useEffect(() => {
        // console.log("Loading complete:", loadingComplete)
        if(loadingComplete){
            setShowButton(true);
        }
    }, [loadingComplete])

    useEffect(() => {
        const intervalId = setInterval(() => {
            setCurrentImageIndex((prevIndex) => (prevIndex % 8) + 1);
        }, 4000);

        return () => {
            clearInterval(intervalId); // Cleanup interval on component unmount
        };
    }, []);

    const getImageUrl = () => `/img/banner${currentImageIndex}.webp`;

    return (
        <>
        {isVisible && (
        <div className="landing">
            <div className="content">
                <div className="firstBloc">
                    <p className="title">Equations en image</p>
                    <div className="secondBloc">
                        <p className="subtitle">Quand se rencontrent le design graphique et les mathématiques</p>
                        <p className="text">
                            Équations en image est un projet mettant en lumière l'association entre art et science 
                            sous la forme d’une série de 8 affiches. Chaque affiche met en avant une équation proposée 
                            par un scientifique du Laboratoire d'informatique Gaspard Monge (LIGM) ou du Laboratoire 
                            d’analyse et de mathématiques appliquées (LAMA). La finalité n’est cependant pas la compréhension 
                            de ces principes scientifiques, mais la recherche graphique qui a permis d’aboutir à ces visuels.
                        </p>
                    </div>
                </div>
                <img className="img_expo" src={getImageUrl()} alt="Illustration de l'exposition"/>
            </div>
            <div className="button">
                {!showButton && <LoadingDots/>}
                {showButton && <div className="show" onClick={handleButtonClick}>Découvrir les affiches</div>}
            </div>
        </div>
        )}
        </>
    )
}

export default Landing;